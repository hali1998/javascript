const express=require('express');
const path=require('path');
const bodyParser=require('body-parser');
const port=process.env.PORT || 2000;


const app=express();

app.use('/static',express.static(path.join(__dirname,'static')));


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.set('view engine','ejs');
app.set('views',path.join(__dirname,'views'));
app.get('/',(req,res)=>{
    // res.send("Hello everyone");
    res.render('index',{title:"First app"});
})

app.post('/',(req,res)=>{
    console.log(req.body);
    res.render('home',{title:"This is home page" ,username:req.body.username,password:req.body.password});
})

app.listen(port,()=>{
    console.log(`server started at http://localhost:${port}`);
})